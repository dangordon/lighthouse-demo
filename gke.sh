# Use the GCP console to create a GKE cluster
# Kubernetes Engine > Create cluster > Node pools (default-pool)
# Change NUMBER OF NODES to 1
# Change MACHINE TYPE to small (1 shared vCPU, 1.7GB memory) aka g1-small
# Click Create to create cluster

# The cluster will take 2-3 minutes to spin up. As it's spinning up, walk the audience
# through all the options available to them in the GKE Cluster Creation UI

# After the cluster is ready, click Connect and click "Run in Cloud Shell"
# Hit Enter right after Cloud Shell pops up to update kubeconfig so we can access this cluster

# Create a simple hello world Deployment onto the cluster
kubectl run hello-app --image gcr.io/google-samples/hello-app:1.0 --port 8080

# Show the audience the resultant pod
kubectl get pods

# Provision an L4 load balancer from Google Cloud (this takes a minute)
kubectl expose deployment hello-app --type=LoadBalancer --port 80 --target-port 8080

# Point out the <pending> external IP for "hello-app" service. Wait till there's a public IP
watch kubectl get service

# Hit the web app from browser by its IP address.

# Create 12 replicas of the pod and watch it
# Point out that only 2 pods are successfully RUNNING; rest are PENDING
kubectl scale deployment hello-app --replicas=12; watch kubectl get pods

# (optional) Show that only 2/12 are available
kubectl get deployment hello-app  

# Resize the cluster itself by adding a node. Wait. Show that 4 additional pods are now RUNNING.
gcloud container clusters resize cluster-1 --zone us-central1-a --size 2; watch kubectl get pods

# (optional) Show that 6/12 are available
kubectl get deployment hello-app

# Resize the cluster again by adding another node. Wait. All 12 pods should now be running.
gcloud container clusters resize cluster-1 --zone us-central1-a --size 3; watch kubectl get pods

# Hit the web app via browser, then update it to v2. You can refresh periodically during rolling update.
kubectl set image deployment hello-app hello-app=gcr.io/google-samples/hello-app:2.0; watch kubectl get pods

# Cleanup (DOESN'T have to be live during demo)
kubectl delete service hello-app
gcloud compute forwarding-rules list
gcloud container clusters delete cluster-1 -z us-central1-a
